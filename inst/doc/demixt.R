## ----setup, include=FALSE, message=FALSE--------------------------------------
library(ggplot2)
library(DeMixT)
# knitr::opts_chunk$set(echo = TRUE)

## ----install_DeMixT-----------------------------------------------------------
# devtools::install_github("wwylab/DeMixT")

## ----Algorithm, echo=FALSE, out.width='100%'----------------------------------
knitr::include_graphics(path = paste0("Algorithm.png"))

## ---- sim_2comp_GS, results="hide", message=FALSE-----------------------------
data("test.data.2comp")
# res.GS = DeMixT_GS(data.Y = test.data.2comp$data.Y, 
#                     data.N1 = test.data.2comp$data.N1,
#                     niter = 30, nbin = 50, nspikein = 50,
#                     if.filter = TRUE, ngene.Profile.selected = 150,
#                     mean.diff.in.CM = 0.25, ngene.selected.for.pi = 150,
#                     tol = 10^(-5))
load('Res_2comp/res.GS.RData')

## ----sim_2comp_GS_res---------------------------------------------------------
head(t(res.GS$pi))
head(res.GS$gene.name)

## ---- sim_2comp_S2, results="hide", message=FALSE-----------------------------
data("test.data.2comp")
# res.S2 <- DeMixT_S2(data.Y = test.data.2comp$data.Y, 
#                     data.N1 = test.data.2comp$data.N1,
#                     data.N2 = NULL, 
#                     givenpi = c(t(res.S1$pi[-nrow(res.GS$pi),])), nbin = 50)
load('Res_2comp/res.S2.RData')

## ----sim_2comp_S2_res---------------------------------------------------------
head(res.S2$decovExprT[,1:5],3)
head(res.S2$decovExprN1[,1:5],3)
head(res.S2$decovMu,3)
head(res.S2$decovSigma,3)

## ----read_data_2comp,  warning=FALSE, message=FALSE---------------------------
# ## DeMixT_S1 without Spike-in Normal
# res.S1 = DeMixT_S1(data.Y = test.data.2comp$data.Y, 
#                    data.N1 = test.data.2comp$data.N1,
#                    niter = 30, nbin = 50, nspikein = 0,
#                    if.filter = TRUE, 
#                    mean.diff.in.CM = 0.25, ngene.selected.for.pi = 150,
#                    tol = 10^(-5))
# ## DeMixT_S1 with Spike-in Normal
# res.S1.SP = DeMixT_S1(data.Y = test.data.2comp$data.Y, 
#                      data.N1 = test.data.2comp$data.N1,
#                      niter = 30, nbin = 50, nspikein = 50,
#                      if.filter = TRUE, 
#                      mean.diff.in.CM = 0.25, ngene.selected.for.pi = 150,
#                      tol = 10^(-5))
# ## DeMixT_GS with Spike-in Normal
# res.GS.SP = DeMixT_GS(data.Y = test.data.2comp$data.Y, 
#                      data.N1 = test.data.2comp$data.N1,
#                      niter = 30, nbin = 50, nspikein = 50,
#                      if.filter = TRUE, ngene.Profile.selected = 150,
#                      mean.diff.in.CM = 0.25, ngene.selected.for.pi = 150,
#                      tol = 10^(-5))
load('Res_2comp/res.S1.RData'); load('Res_2comp/res.S1.SP.RData'); 
load('Res_2comp/res.GS.RData'); load('Res_2comp/res.GS.SP.RData'); 

## ----sim_2comp, fig.height = 4, fig.width = 6, fig.align='center', warning=FALSE----
res.2comp = as.data.frame(cbind(round(rep(t(test.data.2comp$pi[2,]),3),2), 
                                round(c(t(res.S1$pi[2,]),t(res.S1.SP$pi[2,]), t(res.GS.SP$pi[2,])),2),
                                rep(c('S1','S1-SP','GS-SP'), each = 100)), num = 1:2)
res.2comp$V1 <- as.numeric(as.character(res.2comp$V1))
res.2comp$V2 <- as.numeric(as.character(res.2comp$V2))
res.2comp$V3 = as.factor(res.2comp$V3)
names(res.2comp) = c('True.Proportion', 'Estimated.Proportion', 'Method')
## Plot
ggplot(res.2comp, aes(x=True.Proportion, y=Estimated.Proportion, group = Method, color=Method, shape=Method)) +
  geom_point() + 
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "black", lwd = 0.5) +
  xlim(0,1) + ylim(0,1)  +
  scale_shape_manual(values=c(seq(1:3))) +
  labs(x = 'True Proportion', y = 'Estimated Proportion')  

## ----read_data_3comp,  warning=FALSE, message=FALSE---------------------------
data("test.data.3comp")
# res.S1 <- DeMixT_S1(data.Y = test.data.3comp$data.Y, data.N1 = test.data.3comp$data.N1,
#                    data.N2 = test.data.3comp$data.N2, if.filter = TRUE)
load('Res_3comp/res.S1.RData'); 

## ----sim_3comp, fig.height = 4, fig.width = 6, fig.align='center', warning=FALSE----
res.3comp= as.data.frame(cbind(round(t(matrix(t(test.data.3comp$pi), nrow = 1)),2), 
                                round(t(matrix(t(res.S1$pi), nrow = 1)),2), 
                                rep(c('N1','N2','T'), each = 20)))
res.3comp$V1 <- as.numeric(as.character(res.3comp$V1))
res.3comp$V2 <- as.numeric(as.character(res.3comp$V2))
res.3comp$V3 = as.factor(res.3comp$V3)
names(res.3comp) = c('True.Proportion', 'Estimated.Proportion', 'Component')
## Plot
ggplot(res.3comp, aes(x=True.Proportion, y=Estimated.Proportion, group = Component, color=Component, shape=Component)) +
  geom_point() + 
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "black", lwd = 0.5) +
  xlim(0,1) + ylim(0,1)  +
  scale_shape_manual(values=c(seq(1:3))) +
  labs(x = 'True Proportion', y = 'Estimated Proportion')  

## ----read_data_PRAD,  warning=FALSE, message=FALSE----------------------------
load('res.PRAD.RData'); 

## ----PRAD, fig.height = 4, fig.width = 6, fig.align='center', warning=FALSE----
res.PRAD.df = as.data.frame(cbind(res.PRAD$res.GS.PRAD, res.PRAD$res.GS.SP.PRAD))
res.PRAD.df$V1 <- as.numeric(as.character(res.PRAD.df$V1))
res.PRAD.df$V2 <- as.numeric(as.character(res.PRAD.df$V2))
names(res.PRAD.df) = c('Estimated.Proportion', 'Estimated.Proportion.SP')
## Plot
ggplot(res.PRAD.df, aes(x=Estimated.Proportion, y=Estimated.Proportion.SP)) +
  geom_point() + 
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "black", lwd = 0.5) +
  xlim(0,1) + ylim(0,1)  +
  scale_shape_manual(values=c(seq(1:3))) +
  labs(x = 'Estimated Proportion', y = 'Estimated Proportion After Spike-in')  

## -----------------------------------------------------------------------------
sessionInfo(package = "DeMixT")

